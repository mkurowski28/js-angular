## Agnular, TypeScript and Express project.

#####To run the project:

1. Enter `Backend` directory and run:
   1. `yarn install`
   2. `yarn start`
2. Enter `Frontend` directory and run:
    1. `yarn install`
    2. `ng serve`
