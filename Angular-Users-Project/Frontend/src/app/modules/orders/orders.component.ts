import { Component } from '@angular/core';
import { UserService } from '../users/services/user.service';
import { IOrder } from 'models/order.model';
import { Router } from '@angular/router';
@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
})
export class OrdersComponent {
  orders: IOrder[] = [];
  constructor(private userService: UserService, private router: Router) {}
  ngOnInit(): void {
    this.userService.getOrders().subscribe({
      next: (orders) => {
        this.orders = orders;
      },
      error: (error) => {},
      complete: () => {},
    });
  }
  deleteOrder(id: number): void {
    this.userService.deleteOrder(id).subscribe({
      next: (res) => {
        console.log(res.message);
        this.refresh();
      },
      error: (err) => {
        console.log(err.message);
      },
      complete: () => {},
    });
  }
  refresh(): void {
    window.location.reload();
  }
  navToUser(userId: number): void {
    this.router.navigate(['/users/', userId]);
  }
  navToOrder(userId: number, orderId: number) {
    this.router.navigate(['/users/', userId, 'order', orderId]);
  }
}
