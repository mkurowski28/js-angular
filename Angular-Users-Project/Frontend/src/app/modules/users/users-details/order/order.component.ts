import { Component } from '@angular/core';
import { UserService } from '../../services/user.service';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { IOrder } from 'models/order.model';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
})
export class OrderComponent {
  constructor(
    private UserService: UserService,
    private router: Router,
    private route: ActivatedRoute
  ) {}
  order: IOrder;
  ngOnInit(): void {
    this.route.data.subscribe((data: Data) => {
      this.order = data['order'];
      console.log(this.order);
    });
  }
}
