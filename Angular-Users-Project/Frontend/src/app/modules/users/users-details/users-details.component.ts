import { Component } from '@angular/core';
import {
  ActivatedRoute,
  ParamMap,
  Params,
  Route,
  Router,
  Data,
} from '@angular/router';
import { User, IUser } from 'models/user.model';
import { Order, IOrder } from 'models/order.model';
import * as _ from 'lodash';
import { UserService } from '../services/user.service';
import { IMessage, IMessageType } from 'models/messages.model';

UserService;
@Component({
  selector: 'app-users-details',
  templateUrl: './users-details.component.html',
  styleUrls: ['./users-details.component.scss'],
})
export class UsersDetailsComponent {
  constructor(
    private UserService: UserService,
    private router: Router,
    private route: ActivatedRoute
  ) {}
  messages: IMessage[] = [];
  user: IUser;
  orders: IOrder[] = [];
  sum: number;
  ngOnInit(): void {
    this.route.data.subscribe((data: Data) => {
      this.user = data['user'];
      this.orders = data['userOrders'];
      if (this.orders.length > 0)
        this.sum = this.orders.reduce(
          (acc, currVal) => acc + currVal.price * currVal.quantity,
          0
        );
    });
  }
  deleteOrder(id: number): void {
    this.UserService.deleteOrder(id).subscribe({
      next: (res) => {
        console.log(res.message);
        this.refresh();
      },
      error: (err) => {
        console.log(err.message);
      },
      complete: () => {},
    });
  }
  deleteUser(id: number): void {
    this.UserService.deleteUser(id).subscribe({
      next: (res) => {
        console.log(res.message);
        const message: IMessage = {
          id: this.messages.length + 1,
          type: IMessageType.Success,
          text: 'User has been successfully deleted.',
        };
        this.messages.push(message);
        setTimeout(() => {
          this.goToRoot();
        }, 3000);
      },
      error: (err) => {
        //TODO: Recognize the error and serve it.
        const message: IMessage = {
          id: this.messages.length + 1,
          type: IMessageType.Error,
          text: err.message,
        };
        this.messages.push(message);
      },
      complete: () => {},
    });
  }
  goToRoot(): void {
    this.router.navigate([`/users`]);
  }
  refresh(): void {
    window.location.reload();
  }
}
