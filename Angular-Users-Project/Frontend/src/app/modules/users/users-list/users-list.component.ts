import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { User, IUser } from 'models/user.model';
import { Route, Router } from '@angular/router';
@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
})
export class UsersListComponent implements OnInit {
  users: IUser[] = [];
  constructor(private UserService: UserService, private router: Router) {}

  ngOnInit(): void {
    this.UserService.getUsers().subscribe((res: IUser[]) => {
      this.users = res;
    });
  }
  goToDetails(id: number): void {
    this.router.navigate([`/users/`, id]);
  }
}
