import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Order, IOrder } from 'models/order.model';
import { User, IUser } from 'models/user.model';
import { IBackendResponse } from 'models/backendResponse.model';
import { Observable, catchError } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private httpClient: HttpClient) {}
  getUsers(): Observable<IUser[]> {
    return this.httpClient.get<IUser[]>('/api/users');
  }
  getOrders(): Observable<IOrder[]> {
    return this.httpClient.get<IOrder[]>('/api/orders');
  }
  getUserById(id: number): Observable<IUser> {
    return this.httpClient.get<IUser>(`/api/users/${id}`);
  }
  getOrdersByUserId(id: number): Observable<IOrder[]> {
    return this.httpClient.get<IOrder[]>(`/api/users/${id}/orders/`);
  }
  getOrderById(id: number): Observable<IOrder> {
    return this.httpClient.get<IOrder>(`/api/orders/${id}`);
  }
  postUser(name: string, lastName: string): Observable<IUser> {
    return this.httpClient.post<IUser>('/api/user', {
      name: name,
      lastName: lastName,
    });
  }
  postOrder(
    user: IUser,
    orderName: string,
    quantity: number,
    price: number
  ): Observable<IOrder> {
    return this.httpClient.post<IOrder>('/api/order', {
      name: orderName,
      user: user,
      quantity: quantity,
      price: price,
    });
  }
  deleteOrder(id: number): Observable<any> {
    return this.httpClient.delete<any>(`/api/order/${id}`);
  }
  deleteUser(id: number): Observable<any> {
    return this.httpClient.delete<any>(`/api/user/${id}`);
  }
  putUser(id: number, name: string, lastName: string): Observable<IBackendResponse> {
    return this.httpClient.put<IBackendResponse>(`/api/user/${id}`, {
      name: name,
      lastName: lastName,
    });
  }
  putOrder(
    id: number,
    name: string,
    quantity: number,
    price: number
  ): Observable<object> {
    return this.httpClient.put<object>(`/api/user/${id}`, {
      name: name,
      quantity: quantity,
      price: price,
    });
  }
}
