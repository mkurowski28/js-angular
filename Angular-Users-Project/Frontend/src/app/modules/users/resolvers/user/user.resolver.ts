import { inject } from '@angular/core';
import { ResolveFn, Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { IUser,User } from 'models/user.model';
import { DomSanitizer } from '@angular/platform-browser';
export const userResolver: ResolveFn<IUser> = (route) => {
  return inject(UserService).getUserById(+route.params['userId']!);
};