import { TestBed } from '@angular/core/testing';
import { ResolveFn } from '@angular/router';

import { userOrdersResolver } from './user-orders.resolver';

describe('userOrdersResolver', () => {
  const executeResolver: ResolveFn<boolean> = (...resolverParameters) => 
      TestBed.runInInjectionContext(() => userOrdersResolver(...resolverParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeResolver).toBeTruthy();
  });
});
