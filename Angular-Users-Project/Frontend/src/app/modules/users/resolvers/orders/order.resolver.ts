import { inject } from '@angular/core';
import { ResolveFn } from '@angular/router';
import { UserService } from '../../services/user.service';
import { IOrder } from 'models/order.model';

export const orderResolver: ResolveFn<IOrder> = (route, state) => {
  return inject(UserService).getOrderById(+route.params['orderId']!);
};
