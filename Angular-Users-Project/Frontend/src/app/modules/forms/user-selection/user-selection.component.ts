import {
  Component,
  EventEmitter,
  Input,
  Output,
  SimpleChanges,
} from '@angular/core';
import { IUser } from 'models/user.model';
import { FormControl } from '@angular/forms';
import { UserService } from 'src/app/modules/users/services/user.service';

@Component({
  selector: 'app-user-selection',
  templateUrl: './user-selection.component.html',
  styleUrls: ['./user-selection.component.scss', '../../../variables.scss'],
})
export class UserSelectionComponent {
  users: IUser[] = [];
  @Input() control: FormControl;
  @Input() newUser: IUser;
  constructor(private userService: UserService) {}
  ngOnInit(): void {
    this.userService.getUsers().subscribe({
      next: (foundUsers) => {
        this.users = foundUsers;
      },
      error: (error) => {
        console.log(error);
      },
    });
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes['newUser']) {
      const newUser: IUser = changes['newUser'].currentValue;
      const foundUser = this.users.find((user) => user.id === newUser?.id);
      if (foundUser) {
        foundUser.name = newUser.name;
        foundUser.lastName = newUser.lastName;
      } else {
        this.users.push(newUser);
      }
    }
  }
}
