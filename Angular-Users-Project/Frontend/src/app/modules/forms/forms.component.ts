import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { IMessage, IMessageType } from 'models/messages.model';
import { IUser } from 'models/user.model';
@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss', './forms.scss'],
})
export class FormsComponent {
  addFormsActive: boolean=true;
  newUser: IUser = { id: 222, name: 'AAAAAA', lastName: 'BBBBBBBB' };
  newMessage: IMessage;
}
