import { Component, Input, SimpleChanges } from '@angular/core';
import { IMessage, IMessageType } from 'models/messages.model';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss', '../../../variables.scss'],
})
export class MessagesComponent {
  messages: IMessage[] = [];
  @Input() newMessage: IMessage;
  constructor() {}
  ngOnInit() {}
  ngOnChanges(changes: SimpleChanges) {
    if (changes['newMessage']) {
      console.log('found message');

      const newMessage = changes['newMessage'].currentValue;
      const messageId = this.addMessage(newMessage);
      this.removeMessage(messageId);
    }
  }
  addMessage(message: IMessage): number {
    const id = this.messages.reduce((acc, currVal) => {
      if (currVal.id) {
        return acc + currVal.id + 1;
      } else {
        return acc + 1;
      }
    }, 0);
    this.messages.push({
      id: id,
      type: message.type,
      text: message.text,
    });
    return id;
  }
  removeMessage(messageId: number, timeout: number = 3000): void {
    const index = this.messages.findIndex(
      (message) => message.id === messageId
    );
    setTimeout(() => {
      this.messages.splice(index, 1);
    }, timeout);
  }
}
