import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormArray,
} from '@angular/forms';
import { Validators } from '@angular/forms';
import { HttpParams } from '@angular/common/http';
import { Data, Router } from '@angular/router';
import { IMessage, IMessageType } from 'models/messages.model';
import { IUser } from 'models/user.model';
import { UserService } from '../../users/services/user.service';
@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: [
    './add-user.component.scss',
    '../forms.scss',
    '../../../variables.scss',
  ],
})
export class AddUserComponent {
  redirectCheckbox: boolean;
  userForm = this.formBuilder.group({
    user: [-1],
    name: [
      'Name',
      [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
        Validators.pattern('^ *[a-zA-Z]{3,30} *$'),
      ],
    ],
    lastName: [
      'Lastname',
      [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
        Validators.pattern('^ *[a-zA-Z]{3,30} *$'),
      ],
    ],
    redirectCheckbox: [true],
  });
  @Input() addForm: boolean;
  @Input() title: string;
  @Output() usersEmitter = new EventEmitter<IUser>();
  @Output() messagesEmitter = new EventEmitter<IMessage>();

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}
  submit() {
    if (this.addForm) {
      this.addUser();
    } else {
      this.editUser();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['addForm']) {
      this.addForm = changes['addForm'].currentValue;
      if (!this.addForm) {
        this.userForm.controls.user.addValidators([
          Validators.required,
          Validators.min(1),
        ]);
      } else {
        this.userForm.controls.user.removeValidators([
          Validators.required,
          Validators.min(1),
        ]);
      }
    }
  }

  addUser(): void {
    if (this.userForm.valid) {
      const { name, lastName } = this.userForm.value;
      if (!(name && lastName)) return;
      this.userService.postUser(name.trim(), lastName.trim()).subscribe({
        next: (addedUser) => {
          this.usersEmitter.emit(addedUser);
          this.messagesEmitter.emit({
            type: IMessageType.Success,
            text: 'User has been successfully added!',
          });
        },
        error: (err) => {
          this.messagesEmitter.emit({
            type: IMessageType.Error,
            text: `Error: :( ${err.message}`,
          });
        },
        complete: () => {},
      });
    }
  }

  editUser(): void {
    if (this.userForm.valid) {
      const { name, lastName, user } = this.userForm.value;
      if (!(name && lastName && user)) return;
      console.log(user, name, lastName);

      this.userService.putUser(user, name, lastName).subscribe({
        next: (result) => {
          this.messagesEmitter.emit({
            text: "User's data has been successfully changed!",
            type: IMessageType.Success,
          });
          const { id, name, lastName } = result.user!;
          this.usersEmitter.emit({ id: id, name: name, lastName: lastName });
        },
        error: (err) => {
          this.messagesEmitter.emit({
            text: "User's data has been NOT successfully changed!",
            type: IMessageType.Error,
          });
        },
        complete: () => {},
      });
    }
  }
  goToDetails(id: number): void {
    this.router.navigate([`/users/${id}`]);
  }
  refresh(): void {
    window.location.reload();
  }
  get user() {
    return this.userForm.get('user');
  }
  get name() {
    return this.userForm.get('name');
  }
  get lastName() {
    return this.userForm.get('lastName');
  }
}
