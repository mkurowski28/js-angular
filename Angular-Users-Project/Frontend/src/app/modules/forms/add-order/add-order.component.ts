import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IMessage, IMessageType } from 'models/messages.model';
import { IUser, User } from 'models/user.model';
import { IOrder } from 'models/order.model';
import { max } from 'lodash';
import { UserService } from '../../users/services/user.service';
@Component({
  selector: 'app-add-order',
  templateUrl: './add-order.component.html',
  styleUrls: ['./add-order.component.scss', '../forms.scss'],
})
export class AddOrderComponent {
  messages: IMessage[] = [];
  users: IUser[] = [];
  addOrderButtonDisabled: boolean = true;
  addOrderForm = this.formBuilder.group({
    user: [-1, [Validators.required, Validators.min(1)]],
    orderName: [
      'order',
      [Validators.required, Validators.minLength(3), Validators.maxLength(30)],
    ],
    quantity: [
      1,
      [
        Validators.required,
        Validators.pattern('^[1-9]+[0-9]*$'),
        Validators.min(1),
        Validators.max(999),
      ],
    ],
    price: [
      25,
      [
        Validators.required,
        Validators.pattern('^[1-9]+[0-9]*$'),
        Validators.min(1),
        Validators.max(9999),
      ],
    ],
  });
  @Input() title: string;
  @Input() newUser: IUser;
  @Output() newUserEmit = new EventEmitter<IUser>();
  @Output() messagesEmitter = new EventEmitter<IMessage>();

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}
  ngOnInit(): void {
    this.userService.getUsers().subscribe({
      next: (res) => {
        this.users = res;
        this.addOrderForm.statusChanges.subscribe((status) => {
          if (status === 'VALID' || status === 'DISABLED') {
            this.addOrderButtonDisabled = false;
          } else {
            this.addOrderButtonDisabled = true;
          }
        });
      },
      error: () => {
        this.messagesEmitter.emit({
          type: IMessageType.Error,
          text: 'Nie udało się pobrać użytkowników!',
        });
      },
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['newUser']) {
      const newUser: IUser = changes['newUser'].currentValue;
      this.users.push(newUser);
    }
  }

  onSubmit(): void {
    if (this.addOrderForm.valid) {
      // this.addOrderForm.get('xyz').some by manipulować np. inputem
      // addOrderForm.controls do odwoływania się do pól
      const { user, orderName, quantity, price } = this.addOrderForm.value;

      if (this.addOrderForm.valid) {
        if (!user || !orderName || !quantity || !price) {
          console.log('ERROR! OBSZEDŁEŚ WALIDACJE!');
          return;
        }
        this.userService.getUserById(user).subscribe({
          next: (user) => {
            console.log('Udało się uzyskać usera');
            this.userService
              .postOrder(user, orderName, quantity, price)
              .subscribe({
                next: (order) => {
                  this.messagesEmitter.emit({
                    type: IMessageType.Success,
                    text: `Udało się dodać order: ${order.id}`,
                  });
                },
                error: (error) => {
                  this.messagesEmitter.emit({
                    type: IMessageType.Error,
                    text: error.message,
                  });
                },
                complete: () => {},
              });
          },
          error: (error) => {
            this.messagesEmitter.emit({
              type: IMessageType.Error,
              text: `Nie udało się dodać orderu: ${error.message}`,
            });
          },
          complete: () => {},
        });
      } else {
        this.messagesEmitter.emit({
          type: IMessageType.Error,
          text: 'Coś poszło nie tak!',
        });
      }
    }
  }

  get user() {
    return this.addOrderForm.get('user');
  }
  get orderName() {
    return this.addOrderForm.get('orderName');
  }
  get quantity() {
    return this.addOrderForm.get('quantity');
  }
  get price() {
    return this.addOrderForm.get('price');
  }
}
