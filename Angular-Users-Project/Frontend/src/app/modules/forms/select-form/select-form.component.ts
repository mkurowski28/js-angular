import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-select-form',
  templateUrl: './select-form.component.html',
  styleUrls: ['./select-form.component.scss'],
})
export class SelectFormComponent {
  @Output() activeFormEmitter = new EventEmitter<boolean>();
  addFormsActive: boolean = true;
  constructor() {}
  activeFormsToggle() {
    this.addFormsActive = !this.addFormsActive;
    this.activeFormEmitter.emit(this.addFormsActive);
  }
}
