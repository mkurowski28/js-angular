import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { AccountComponent } from './account/account.component';
import { UsersListComponent } from './users/users-list/users-list.component';
import { UsersDetailsComponent } from './users/users-details/users-details.component';
import { PageNotFoundComponent } from '../core/components/page-not-found/page-not-found.component';
import { userResolver } from './users/resolvers/user/user.resolver';
import { userOrdersResolver } from './users/resolvers/user-orders/user-orders.resolver';
import { OrderComponent } from './users/users-details/order/order.component';
import { orderResolver } from './users/resolvers/orders/order.resolver';
import { OrdersComponent } from './orders/orders.component';
import { FormsComponent } from './forms/forms.component';
const routes: Routes = [
  {
    path: '',
    component: UsersListComponent,
  },
  {
    path: 'users',
    component: UsersListComponent,
  },
  {
    path: 'orders',
    component: OrdersComponent,
  },
  {
    path: 'users/:userId',
    component: UsersDetailsComponent,
    resolve: { user: userResolver, userOrders: userOrdersResolver },
  },
  {
    path: 'account',
    component: AccountComponent,
  },
  {
    path: 'forms',
    component: FormsComponent,
  },
  {
    path: 'users/:userId/order/:orderId',
    component: OrderComponent,
    resolve: {
      order: orderResolver,
    },
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
