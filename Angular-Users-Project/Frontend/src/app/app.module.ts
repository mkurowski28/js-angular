import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './modules/app-routing.module';
import { AppComponent } from './app.component';
import { AccountComponent } from './modules/account/account.component';
import { NavbarComponent } from './core/components/navbar/navbar.component';
import { FooterComponent } from './core/components/footer/footer.component';
import { UsersDetailsComponent } from './modules/users/users-details/users-details.component';
import { UsersListComponent } from './modules/users/users-list/users-list.component';
import { PageNotFoundComponent } from './core/components/page-not-found/page-not-found.component';
import { OrderComponent } from './modules/users/users-details/order/order.component';
import { ReactiveFormsModule } from '@angular/forms';
import { StatusPipe } from './modules/users/pipes/status.pipe';
import { OrdersComponent } from './modules/orders/orders.component';
import { AddUserComponent } from './modules/forms/add-user/add-user.component';
import { FormsComponent } from './modules/forms/forms.component';
import { AddOrderComponent } from './modules/forms/add-order/add-order.component';
import { MessagesComponent } from './modules/forms/messages/messages.component';
import { SelectFormComponent } from './modules/forms/select-form/select-form.component';
import { UserSelectionComponent } from './modules/forms/user-selection/user-selection.component';

@NgModule({
  declarations: [
    AppComponent,
    AccountComponent,
    NavbarComponent,
    FooterComponent,
    UsersDetailsComponent,
    UsersListComponent,
    PageNotFoundComponent,
    AddUserComponent,
    OrderComponent,
    StatusPipe,
    FormsComponent,
    AddOrderComponent,
    OrdersComponent,
    UserSelectionComponent,
    MessagesComponent,
    SelectFormComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
