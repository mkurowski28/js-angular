export interface IMessage {
  id?:number;
  type: IMessageType;
  text: string;
}
export enum IMessageType {
  Success = 1,
  Error = 0,
}
