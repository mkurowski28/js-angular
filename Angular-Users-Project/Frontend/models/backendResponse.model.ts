import { IOrder } from './order.model';
import { IUser } from './user.model';

export interface IBackendResponse {
  message: string;
  user?: IUser;
  order?: IOrder;
}
