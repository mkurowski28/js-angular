export interface IUser {
  id?: number;
  name: string;
  lastName: string;
}
export class User implements IUser {
  id: number = 0;
  name: string = '';
  lastName: string = '';
}
