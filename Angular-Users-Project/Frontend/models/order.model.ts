import { IUser, User } from './user.model';

export interface IOrder {
  id: number;
  name: string;
  user: IUser;
  price: number;
  quantity: number;
}
export class Order implements IOrder {
  id: number = 0;
  name: string = '';
  user: User = new User();
  price: number = 0;
  quantity: number = 0;
}
