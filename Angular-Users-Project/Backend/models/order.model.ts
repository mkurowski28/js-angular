import { User } from './user.model';

export interface Order {
  id: number;
  name: string;
  user: User;
  price: number;
  quantity: number;
}
