import { users } from './users';

export const orders = [
  {
    'id': 1,
    'name': 'pralka',
    'user': users[0],
    'price': 79,
    'quantity': 8
  },
  {
    'id': 2,
    'name': 'drukarka $',
    'user': users[1],
    'price': 70,
    'quantity': 6
  },
  {
    'id': 3,
    'name': 'heroina',
    'user': users[2],
    'price': 79,
    'quantity': 8
  },
  {
    'id': 4,
    'name': 'łapówki',
    'user': users[2],
    'price': 70,
    'quantity': 6
  }
];
