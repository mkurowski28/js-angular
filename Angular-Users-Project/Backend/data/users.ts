import { User } from "models/user.model";
export const users:User[] =  [
  {
    id: 1,
    name: "User 1",
    lastName: "Last Name 1"
  },
  {
    id: 2,
    name: "User 2",
    lastName: "Last Name 2"
  },
  {
    id: 3,
    name: "User 3",
    lastName: "Last Name 3"
  },
  {
    id: 4,
    name: "User 4",
    lastName: "Last Name 4"
  },
  {
    id: 5,
    name: "User 5",
    lastName: "Last Name 5"
  }
];