import { Order } from '../models/order.model';
import { User } from '../models/user.model';
import express, { Request, Response } from 'express';
import { users } from '../data/users';
import { orders } from '../data/orders';
// const express = require("express");
const cors = require('cors');
const app = express();
const port = 3000;

app.use(cors());
app.use(express.json());

app.get('/api/', (req: Request, res: Response): void => {
  res.send('Hello World!');
});

app.get('/api/users', (req: Request, res: Response): void => {
  res.send(users);
});

app.get('/api/users/:id', (req: Request, res: Response): void => {
  const id = +req.params.id;
  const user = users.find((user) => user.id === id);
  console.log(user);
  if (!user) {
    res.status(404).send('0 users found');
    return;
  }
  res.send(user);
});

app.get('/api/users/:id/orders/', (req: Request, res: Response): void => {
  const id = +req.params.id;
  const user = users.find((user) => user.id === id);
  if (!user) {
    res.status(404).send('no user with that ID found');
    return;
  }
  const userOrders = orders.filter((order) => order.user.id === user.id);
  console.log(userOrders, 1);

  res.status(200).send(userOrders);
});

app.get('/api/orders', (req: Request, res: Response): void => {
  res.send(orders);
});
app.get('/api/orders/:id', (req: Request, res: Response): void => {
  const id = +req.params.id;
  const foundOrder = orders.find((order) => order.id == id);
  if (!foundOrder) {
    res.status(404).send('Brak orderu o takim ID');
    return;
  }
  res.send(foundOrder);
});

app.post('/api/user', (req: Request, res: Response): void => {
  console.log(req.body);

  if (!req.body.name || !req.body.lastName) {
    res.status(400).send('Brak name lub lastname');
    return;
  }
  const newId = users.reduce((acc, currVal) => currVal.id + 1, 0);
  const user: User = {
    id: newId,
    name: req.body.name,
    lastName: req.body.lastName
  };
  users.push(user);
  console.log('dodano:', user);

  res.status(200).send(user);
  return;
});

app.post('/api/order', (req: Request, res: Response): void => {
  const { name, user, quantity, price } = req.body;
  if (name && user && quantity && price) {
    const order: Order = {
      id: orders.reduce((acc, currVal) => currVal.id + 1, 0),
      name: name,
      user: user,
      price: price,
      quantity: quantity
    };
    orders.push(order);
    res.status(200).send(order);
    return;
  }
  res.status(500).send({ message: 'ERROR!1!' });
});

app.put('/api/user/:id', (req: Request, res: Response): void => {
  const id = +req.params.id;
  const { name, lastName } = req.body;
  const user = users.find((user) => user.id === id);
  if (!user) {
    res.status(404).send('No user found');
    console.log(id, req.body);
    return;
  }
  user.name = name;
  user.lastName = lastName;
  res.status(200).send({ message: "User's data has been changed", user: user });
});

app.delete('/api/user/:id', (req: Request, res: Response): void => {
  const id = +req.params['id'];
  const user = users.find((user) => user.id == id);

  if (!user) {
    res.status(404).send('Taki user nie istnieje');
    return;
  }
  const index = users.findIndex((userInner: User) => userInner.id === user.id);
  users.splice(index, 1);
  const ordersToDelete = orders.filter((order: Order) => order.user.id === user.id);
  ordersToDelete.forEach((toDelete: Order) => {
    const index = orders.findIndex((order) => order.id === toDelete.id);
    orders.splice(index, 1);
  });
  res.status(200).send({ message: `User: ${user} został usunięty \n ${users}` });
});
app.delete('/api/order/:id', (req: Request, res: Response) => {
  const id = +req.params['id'];
  const foundOrder = orders.find((order: Order) => order.id === id);
  if (!foundOrder) {
    console.log(id);
    res.status(404).send({ message: 'Brak orderu o takim id' });
    return;
  }
  const index = orders.findIndex((order: Order) => order.id === foundOrder.id);
  orders.splice(index, 1);
  res.status(200).send({ message: 'udalo sie usunac order' });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
