import requests
# obj = {"firstName":"jeden","lastName":"dwa"}
obj = {"name":"jeden","lastName":"dwa"}
def post(data):
    zmienna = requests.post("http://localhost:3000/user",json=data)
    return zmienna.text
    
def put(data):
    zmienna = requests.put("http://localhost:3000/user/2",json=data)
    return zmienna.text

def delete(data):
    zmienna = requests.delete("http://localhost:3000/user/2",json=data)
    return zmienna.text

def patch(data):
    zmienna = requests.patch("http://localhost:3000/user/2",json=data)
    return zmienna.text



# print(post(obj))
# print(put(obj))
print(delete(obj))